# Indice
- [Indice](#indice)
- [GUEST](#guest)
- [Reporter](#reporter)
- [Developer](#developer)
- [Mantainer](#mantainer)
# GUEST
* **Puede** ver el código aunque sea privado,
* **Puede** clonar el proyecto 
* **Puede** realizar pulls, 
* **Puede** realizar issues pero **NO** asignar encargado, 
* **Puede** añadir codigo y commits pero solo localmente, 
* **NO** puede añadir código o ramas o hacer a un "push" al repositorio remoto. 
* **NO** puede hacer merge request.
* **NO** puede hacer push
* **NO** puede modificar permisos

# Reporter
* **Puede** ver el código aunque sea privado,
* **Puede** clonar el proyecto 
* **Puede** realizar pulls, 
* **Puede** realizar issues **Y** asignar encargado, 
* **Puede** añadir codigo y commits pero solo localmente, 
* **NO** puede añadir código o ramas o hacer a un "push" al repositorio remoto. 
* **Puede** cerrar issues
* **NO** puede hacer merge request.
* **NO** puede hacer push
* **NO** puede modificar permisos

# Developer
* **Puede** clonar y ver el repositorio,
* **Puede** crear issues,
* **Puede** crear ramas en el remoto
* **Puede** hacer commits y branchs
* **Puede** realizar pull request
* **Puede** realizar pull
*  **Puede** asignar encargados issue
* **Puede** realizar push en sus propias ramas
* **NO** puede realizar "push"  a la rama master o las q no son suyas
* **NO** puede ACEPTAR merge en el master
* **Puede** aceptar merge si las ramas involucrados fueron creadas por el mismo
* **Puede** cerrar issues
* **Puede** aprobar merges

# Mantainer
* **Puede** clonar y ver el repositorio,
* **Puede** crear issues,
* **Puede** crear ramas en el remoto
* **Puede** hacer commits y branchs
* **Puede** realizar pull
* **Puede** realizar pull request
* **Puede** realizar push en sus propias ramas
* **Puede** realizar "push"  a la rama master o las q no son suyas
* **Puede** ACEPTAR merge en el master
* **Puede** cerrar issues
* **Puede** aprobar merges incluso en el master
* **Puede** cambiar los permisos del repositorio
* **Puede** renombrar el repositorio
* **Puede** asignar encargados issue