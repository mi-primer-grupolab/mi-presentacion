## Carlos Gutierrez / estudiante Ingeniería en Sistemas Informaticos y de la Computación

![foto-erfil](foto-perfil.PNG)


El software se encuentra presente en muchos aspectos de la vida, desde alarmas hasta sofisticadas aplicaciones que permiten viajes espaciales. Realizar software que ayude a la gente a mejorar o facilitar su estilo de vida es mi objetivo. Soy una persona que disfruta de las cosas sencillas, salir a caminar, comer un postre o leer un buen manga. Actualmente soy un estudiante de la EPN que trata de aprender todo lo posible para a travez del software colaborar con el bienestar de las personas. Un poco más acerca de mi 
[clic aqui](INSTITUCIONES.md)
